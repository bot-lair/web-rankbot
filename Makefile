.PHONY: run
run: build
	cd app; node build/bot.js

.PHONY: install
install:
	cd app; npm install

.PHONY: build
build: install
	cd app; tsc

.PHONY: clean
clean:
	rm -rf app/build app/node_modules
