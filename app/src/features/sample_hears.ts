/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */
import { Botkit, BotkitMessage } from "botkit";

module.exports = function (controller: Botkit) {

    // use a function to match a condition in the message
    const hearFn = async (message: BotkitMessage) => {
        if (message.text === undefined) {
            return false;
        }
        if (message.text.toLowerCase() === 'foo') {
            return true;
        }
        return false;
    };

    controller.hears(hearFn, ['message'], async (bot, message) => {
        await bot.reply(message, 'I heard "foo" via a function test');
    });

    // use a regular expression to match the text of the message
    controller.hears(new RegExp(/^\d+$/), ['message', 'direct_message'], async function (bot, message) {
        await bot.reply(message, { text: 'I heard a number using a regular expression.' });
    });

    // match any one of set of mixed patterns like a string, a regular expression
    controller.hears(['allcaps', new RegExp(/^[A-Z\s]+$/)], ['message', 'direct_message'], async function (bot, message) {
        await bot.reply(message, { text: 'I HEARD ALL CAPS!' });
    });

}