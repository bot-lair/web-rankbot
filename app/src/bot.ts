//  __   __  ___        ___
// |__) /  \  |  |__/ |  |  
// |__) \__/  |  |  \ |  |  

// This is the main file for the slack rankbot bot.

// Import Botkit's core features
import { Botkit } from 'botkit';
import { BotkitCMSHelper } from 'botkit-plugin-cms';

// Import a platform-specific adapter for web.

import { WebAdapter } from 'botbuilder-adapter-web';

// import { MongoDbStorage } 'botbuilder-storage-mongodb';

// Load process.env values from .env file 
// import * as dotenv from 'dotenv';
// dotenv.config();

let storage = null;
// if (process.env.MONGO_URI) {
//     storage = mongoStorage = new MongoDbStorage({
//         url: process.env.MONGO_URI,
//     });
// }

const adapter = new WebAdapter({});


const controller = new Botkit({
    webhook_uri: '/api/messages',

    adapter: adapter,

    // storage
});

if (process.env.CMS_URI) {
    controller.usePlugin(new BotkitCMSHelper({
        uri: process.env.CMS_URI,
        token: String(process.env.CMS_TOKEN),
    }));
}

// Once the bot has booted up its internal services, you can use them to do stuff.
controller.ready(() => {

    // load traditional developer-created local custom feature modules
    controller.loadModules(__dirname + '/features');

    /* catch-all that uses the CMS to trigger dialogs */
    if (controller.plugins.cms) {
        controller.on('message,direct_message', async (bot, message) => {
            let results = false;
            results = await controller.plugins.cms.testTrigger(bot, message);

            if (results !== false) {
                // do not continue middleware!
                return false;
            }
        });
    }

});





